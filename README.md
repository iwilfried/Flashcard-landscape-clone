# Flutter Flashcard-Landscape

Flutter is Google's UI toolkit for building beautiful, natively compiled applications for mobile, web, and desktop from a single codebase. Flutter works with existing code, is used by developers and organizations around the world, and is free and open source.  

With Flutter comes a new language, **Dart**, which is a programming language optimized for building user interfaces with features such as the spread operator for expanding collections, and collection if for customizing UI for each platform. 

Flutter is a cool technology, and it’s going to fly. Here’s why.  

![image](https://user-images.githubusercontent.com/1112242/147612102-ed984832-020e-4deb-8d01-70f9ec9d20b2.png)  
https://flutter.dev/   

In collaboration with Google, some of the world’s big players have already created apps using Flutter:

Alibaba – the world's biggest online commerce company, which has 50M+ downloads
Tencent – a video streaming service with tens of millions of MAU.
The New York Times – which runs with the same code on Android, iOS, web, Mac, and Chrome OS.
My suggestions for Flutter use cases:

Use Flutter if you have to ship something quickly and with a limited budget for IOS and Android. While Dart is a considerably new thing, moving from the native languages (Kotlin/Java/Swift) should be relatively easy for native development teams to jump into. Flutter also requires less native code than React Native.
Flutter can also come in handy if you need to create a cross-platform POC/MVP. Surely, if you consider going for native development in the longer run, it is worth considering starting with just Flutter and checking if reality meets the expectations specified in the POC.  



### It’s Native
Well sort of. Flutter’s Skia rendering engine ensures that your app mimics a native look and feel.
Regardless, you cut your time in half when coding with Flutter. You update one code base. This keeps things cheap and organized!
### It’s Not HTML
The way components wrap each other is very familiar to people who know HTML. For visual learners, like myself, this is awesome. It takes that very easy to visualize and organize structure, but is so much better than HTML.
### Support Is Everywhere
The Flutter community is just a-buzzin with enthusiasm and excitement. Flutter on StackOverflow, YouTube, LinkedIn, and Medium is where I personally engage with the community and the help I get is great. There’s so many people who just love to talk about flutter and all the cool features it has and what we can do with them. It’s a party and everyone is invited.
### Flutter’s Backing
It’s Google. They’re the big dogs. They’re not going anywhere. You can depend on bug fixes and updates that are going to be quick and thorough.
Watch Your Language
Dart, which is the language used for Flutter is developed by Google and is super easy to pick up, especially if you know JS.
Quick disclaimer — I personally think that learning new coding languages is really easy, but I promise dart IS actually very easy to learn.
### Clients Want It
The economic and timing benefits are too good to give up. Why would you want to pay more money the same app to take twice as much time to build?
When giving clients quotes for sprints, it’s a great technology to easily build extremely good-looking apps that clients will think you spent hundreds of hours designing, but it’s already built in with Material Design!
For start ups, and creating proof-of-concept NOTHING could be better.
### Bottom Line: Flutter Is a Dope Technology
All of the above may lead you to believe that Flutter may be the best choice for your project. The only thing that can be a problem is Firebase, which is a false friend. It’s very easy to integrate with Flutter, but it is annoying to deal with Firebase itself. Good news though, you don’t even have to use it!
Have you used Flutter? Do you like it? Let me know!
